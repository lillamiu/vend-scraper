# vend scraper

## how to run
1. cd to project folder
2. run `node scraper.js`
3. enter the search term that you want search vend.se for

## todo
- [x] put on gitlab
- [x] have user input for search term (readline? https://nodejs.org/api/readline.html -> worked like a charm)
- [ ] setup email notification 
	- check this out -> http://caspian.dotconf.net/menu/Software/SendEmail/
- [ ] setup cron job
- [ ] wrap everything in an executable/docker/something?
- [x] also use 'Fira Code' as font family for nice ligatures ;) someone else who cares about ligatures! :DD


## chat section
- repo https://gitlab.com/lillamiu/vend-scraper