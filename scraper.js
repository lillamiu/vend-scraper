const puppeteer = require('puppeteer');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'what do you want to search for? '
});

let searchVend = async (searchTerm) => {
  const browser = await puppeteer.launch();

  let urlBuilder = (pageNumber, searchTerm) => {
    let url = "https://beta.vend.se/sok?p=" + pageNumber + "&q=" + searchTerm + "&ct=&c=&t=&co=&o=&s=";
    return url
  }

  const extractItems = async(pageNumber) => {
    const page = await browser.newPage();
    let url = urlBuilder(pageNumber, searchTerm);
    await page.goto(url);
    console.log("------ scraping: " + url + "------------------");
    let items = await page.evaluate(
      () => Array.from(document.querySelectorAll('.list-item'))
        .map(item => ({
          name: item.querySelector('.item-heading').textContent,
          link: item.querySelector('.listing-link').href
        }))
    );
    //should we end recursion?
    if(items.length < 1) {
      return items
    } else {
      //fetch next page
      return items.concat(await extractItems(pageNumber+1))
    }
    //await page.close(); //needed?
  };

  let result = await extractItems(1)

  console.log(result);

  await browser.close();
}


rl.prompt();
rl.on('line', async(line) => {
  console.log('Searching vend for ' + line.trim());
  await searchVend(line.trim());
  process.exit(0);
})